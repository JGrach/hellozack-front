const express = require('express'),
  app = express()

if(!process.env.NODE_ENV) process.env.NODE_ENV = 'development'
const { client: { port }} = require('./config.json')[process.env.NODE_ENV],
  { createHTML } = require('./helpers')

app.use(express.static('static'))

app.get('/', (req, res) => {
  createHTML('home').then( file => {
    res.send(file)
  })
})

app.get('/signalements', (req, res) => {
  createHTML('signalement').then( file => {
    res.send(file)
  })
})

app.listen(process.env.PORT || port)