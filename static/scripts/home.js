signalementInputs().then( function(inputs){
  const labels = document.getElementsByTagName('label')
  for(let i=0; i<labels.length; i++){
    if(labels[i].id !== ''){
      labels[i].innerHTML += inputs[labels[i].id]
    }
  }
})

const formulaire = document.getElementById("newSignalement")
formulaire.addEventListener('submit', function(e){
  e.preventDefault()
  const signalement = {
    date: formulaire.getElementsByClassName('inputDate')[0].value,
    creneau: [formulaire.getElementsByClassName('inputCreneau1')[0].value, formulaire.getElementsByClassName('inputCreneau2')[0].value],
    alerteur: formulaire.getElementsByClassName('inputAlerteur')[0].value,
    animal_id: formulaire.getElementsByClassName('inputAnimal')[0].value,
    couleur: formulaire.getElementsByClassName('inputCouleur')[0].value,
    adresse: formulaire.getElementsByClassName('inputAdresse')[0].value,
    etat_id: formulaire.getElementsByClassName('inputEtat')[0].value,
    collier: formulaire.getElementsByClassName('inputCollier')[0].value,
  }
  postSignalement(signalement).then( function(res){
    alert("le message a bien été envoyé.")
    location.reload()
  })
})