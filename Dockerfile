FROM node:9.11.1-stretch

RUN mkdir /app
WORKDIR /app
ADD . ./
RUN npm install

ENTRYPOINT ["npm", "run"]
CMD ["start"]
