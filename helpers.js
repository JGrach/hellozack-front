const fs = require('fs')
const { api } = require('./config.json')[process.env.NODE_ENV]

function readFilePromise(path){
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8',(err, res) => {
      if(err) return reject(err)
      return resolve(res)
    })
  })
}

function createHTML(name){
  const main = readFilePromise('./filesDom/main.html')
  const menu = readFilePromise('./filesDom/menu.html')
  const html = readFilePromise('./filesDom/' + name + '.html')
  return Promise.all([main, html, menu]).then( document => {
    let mainFile = document[0]
    const htmlFile = document[1]
    const menuFile = document[2]
    mainFile = mainFile.replace('<!--pageCss-->', '<link rel=stylesheet type=text/css href=/css/' + name + '.css></link>')
    mainFile = mainFile.replace('<!--pageScript-->', '<script type=text/javascript src=/scripts/' + name + '.js></script>')
    mainFile = mainFile.replace('<!--menu-->', menuFile)
    mainFile = mainFile.replace('<!--page-->', htmlFile)
    mainFile = mainFile.replace('/*HOST*/', 'const API = { host:"' + api.host + '"};')
    return mainFile
  })
} 

module.exports = {
  createHTML
}