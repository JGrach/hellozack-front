function signalementInputs(signalement){
  const statutsProm = getStatuts()
  const brigadesProm = getBrigades()
  const animauxProm = getAnimaux()
  const etatsProm = getEtats()

  return Promise.all([statutsProm, brigadesProm, animauxProm, etatsProm]).then( function(results) {
    const statuts = results[0]
    const brigades = results[1]
    const animaux = results[2]
    const etats = results[3]
  

    function inputDate(){
      return "<input class='inputDate' type=date " + (signalement && signalement.date ? "value=" + signalement.date : '') + "></input>"
    }

    function optionCreneau(i){
      let signalement_creneau = signalement && signalement.creneau ? signalement.creneau.split(' - ') : null
      const creneau = []
      for(let i=0; i<24; i++){
        creneau[i] = i
      }
      return creneau.map( function(hour){
        return "<option value=" + hour + " " + ( signalement_creneau && signalement_creneau[i] === hour + 'H' ? "selected" : '' ) + ">" + hour + "</option>"
      }).join()
    }

    function inputCreneau(){
      return "<div><select class='inputCreneau1'>" + optionCreneau(0) + "</select>H - <select class='inputCreneau2'>" + optionCreneau(1) + "</select>H</div>"
    }

    function inputAlerteur(){
      return "<input class=inputAlerteur type=mail" + (signalement && signalement.alerteur ? " value=" + signalement.alerteur : '') + "></input>"
    }  

    function optionAnimal(){
      return animaux.map( function(animal){
        return "<option value=" + animal.id + " " + (signalement && signalement.animal && signalement.animal === animal.nom ? "selected" : '') + ">" + animal.nom + "</option>"
      }).join()
    }
    
    function inputAnimal(){
      return "<select class=inputAnimal>" + optionAnimal() + "</select>"
    }  

    function inputCouleur(){
      return "<input class=inputCouleur type=text " + (signalement && signalement.couleur ? "value=" + signalement.couleur : '') + "></input>"
    }

    function inputAdresse(){
      return '<input class=inputAdresse type=text ' + (signalement && signalement.adresse ? ' value="' + signalement.adresse + '"' : "") + '></input>'
    }

    function optionEtat(){
      return etats.map( function(etat){
        return "<option value=" + etat.id + " " + (signalement && signalement.etat && signalement.etat === etat.nom ? "selected" : '') + ">" + etat.nom + "</option>"
      }).join()
    }

    function inputEtat(){
      return "<select class='inputEtat'>" + optionEtat() + "</select>"
    }

    function optionCollier(){
      return [true, false].map(function(collier){
        return "<option value=" + collier + " " + (signalement && signalement.collier && signalement.collier === collier ? "selected" : '') + ">" + (collier ? 'Oui' : 'Non') + "</option>"
      }).join()
    }

    function inputCollier(){
      return "<select class='inputCollier'>" + optionCollier() + "</select>"
    }

    function optionBrigade(){
      brigades.unshift({id: null, nom: ' '})
      return brigades.map( function(brigade){
        return "<option value=" + brigade.id + " " + (signalement && signalement.brigade && signalement.brigade === brigade.nom ? "selected" : '') + ">" + brigade.nom + "</option>"
      }).join()
    }

    function inputBrigade(){
      return "<select class='inputBrigade'>" + optionBrigade() + "</select>"
    }

    function optionStatut(){
      return statuts.map( function(statut){
        return "<option value=" + statut.id + " " + (signalement && signalement.statut && signalement.statut === statut.nom ? "selected" : '') + ">" + statut.nom + "</option>"
      }).join()
    }

    function inputStatut(){
      return "<select class='inputStatut'>" + optionStatut() + "</select>"
    }

    function buttonValider(){
      if(!signalement || !signalement.id) return ''
      return "<button id=valid" + signalement.id + " class='valider'>Valider</button>"
    }

    return {
      adresse: inputAdresse(),
      alerteur: inputAlerteur(),
      animal: inputAnimal(),
      brigade: inputBrigade(),
      collier: inputCollier(),
      couleur: inputCouleur(),
      creneau: inputCreneau(),
      date: inputDate(),
      etat: inputEtat(),
      statut: inputStatut(),
      button: buttonValider()
    }
  })
}
