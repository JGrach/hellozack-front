function errorHdlr(promise){
  return promise.then(function(res) {
    return res.json()
  })
  .then(function(res) {
    if (res && res.error) throw res.error
    return res.data
  })
  .catch(function(err) {
    return alert(err.message || err)
  })
}

function getHeaders() {
  const headers = new Headers
  headers.append('Content-Type', 'application/json')
  headers.append('mode', 'cors')
  return headers
}

function getSignalements(constraints) {
  const headers = getHeaders()
  let query = ''
  const options = {
    method: 'GET',
    headers,
  }
  if(constraints){
    constraints = Object.keys(constraints).map( function(key){
      return key + '=' + constraints[key]
    })
    query = constraints.join('&')
    query = '?'+query
  }
  return errorHdlr(fetch(API.host + '/signalements'  + query, options))
}

function getStatuts() {
  const headers = getHeaders()
  const options = {
    method: 'GET',
    headers,
  }
  return errorHdlr(fetch(API.host + '/statuts', options))
}

function getBrigades() {
  const headers = getHeaders()
  const options = {
    method: 'GET',
    headers,
  }
  return errorHdlr(fetch(API.host + '/brigades', options))
}

function getEtats() {
  const headers = getHeaders()
  const options = {
    method: 'GET',
    headers,
  }
  return errorHdlr(fetch(API.host + '/etats', options))
}
function getAnimaux() {
  const headers = getHeaders()
  const options = {
    method: 'GET',
    headers,
  }
  return errorHdlr(fetch(API.host + '/animaux', options))
}

function postSignalement(signalement) {
  const headers = getHeaders()
  const options = {
    method: 'POST',
    headers,
    body: JSON.stringify(signalement),
  }
  return errorHdlr(fetch(API.host + '/signalement', options))
}

function putSignalement(signalement) {
  const signalement_id = signalement.id
  delete signalement.id
  const headers = getHeaders()
  const options = {
    method: 'PUT',
    headers,
    body: JSON.stringify(signalement),
  }
  return errorHdlr(fetch(API.host + '/signalement/' + signalement_id, options))
}


