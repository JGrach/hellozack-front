const statutsProm = getStatuts()
const brigadesProm = getBrigades()
const brigadeDOM = document.getElementById('brigade')
const statutDOM = document.getElementById('statut')

function pullSignalements(){
  const statut = statutDOM.value
  if(statut === 'tous'){
    return getSignalements()
  } else {
    return getSignalements({statut_id: statut})
  }
}

function listSignalementsDisplay(rewrite = false, promise){
  const signalements = promise || pullSignalements()
  signalements.then( function(signalements){
    if(rewrite) signalementContent.innerHTML = ''
    signalements.forEach( signalement => {
      signalementContent.innerHTML += signalementString(signalement)
    })
    const modifDOM = document.getElementsByClassName('modif')
    for(let i=0; i<modifDOM.length; i++){
      modifDOM[i].addEventListener('click', modification)
    }
  })
}

function signalementString(signalement){
  const date = new Date(signalement.date)
  return "<tr id=" + signalement.id + ">" + 
    "<td class=date>" + date.getDate() + '/' + ((date.getMonth() < 10) ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + '/' + date.getFullYear() + "</td> \n" +
    "<td class=creneau>" + signalement.creneau + "</td> \n" +
    "<td class=alerteur>" + signalement.alerteur + "</td> \n" +
    "<td class=animal>" + signalement.animal + "</td> \n" +
    "<td class=couleur>" + signalement.couleur + "</td> \n" +
    "<td class=adresse>" + signalement.adresse + "</td> \n" +
    "<td class=etat>" + signalement.etat + "</td> \n" +
    "<td class=collier>" + (signalement.collier ? 'Oui' : 'Non') + "</td> \n" + 
    (function(){
      if(!signalement.brigade) return '<td class=brigade></td>'
      return "<td class=brigade>" + signalement.brigade + "</td> \n"
    })() +
    "<td class=statut>" + signalement.statut + "</td>" +
    "<td class=button><button id=modif" + signalement.id + " class=modif>Modifier</button></td>" +
  "</tr>"
}

function signalementModifDisplay(signalement){
  const td = document.getElementById(signalement.id).getElementsByTagName('td')
  signalementInputs(signalement).then( function(inputs){
    for(let i = 0; i<td.length; i++){
      td[i].innerHTML = inputs[td[i].className]
      if(td[i].className === 'button'){
        td[i].getElementsByClassName('valider')[0].addEventListener('click', sendModif)
      }
    }
  })
}

function sendModif(e){
  const signalement_id = e.target.id.split('valid')[1]
  const row = document.getElementById(signalement_id)
  const signalement = {
    id: signalement_id,
    date: row.getElementsByClassName('inputDate')[0].value,
    creneau: [row.getElementsByClassName('inputCreneau1')[0].value, row.getElementsByClassName('inputCreneau2')[0].value],
    alerteur: row.getElementsByClassName('inputAlerteur')[0].value,
    animal_id: row.getElementsByClassName('inputAnimal')[0].value,
    couleur: row.getElementsByClassName('inputCouleur')[0].value,
    adresse: row.getElementsByClassName('inputAdresse')[0].value,
    etat_id: row.getElementsByClassName('inputEtat')[0].value,
    collier: row.getElementsByClassName('inputCollier')[0].value,
    statut_id: row.getElementsByClassName('inputStatut')[0].value,
  }
  if(row.getElementsByClassName('inputBrigade')[0].value !== "null"){
    signalement.brigade_id = row.getElementsByClassName('inputBrigade')[0].value
  }
  putSignalement(signalement).then(function(result){
    listSignalementsDisplay(true)
  })

}

function modification(e){
  const signalement_id = e.target.id.split('modif')[1]
  const row = document.getElementById(signalement_id)
  const signalement = {
    id: signalement_id,
    date: row.getElementsByClassName('date')[0].innerHTML,
    creneau: row.getElementsByClassName('creneau')[0].innerHTML,
    alerteur: row.getElementsByClassName('alerteur')[0].innerHTML,
    animal: row.getElementsByClassName('animal')[0].innerHTML,
    couleur: row.getElementsByClassName('couleur')[0].innerHTML,
    adresse: row.getElementsByClassName('adresse')[0].innerHTML,
    etat: row.getElementsByClassName('etat')[0].innerHTML,
    collier: row.getElementsByClassName('collier')[0].innerHTML,
    brigade: row.getElementsByClassName('brigade')[0] ? row.getElementsByClassName('brigade')[0].innerHTML : null,
    statut: row.getElementsByClassName('statut')[0].innerHTML,
  }
  signalement.collier = signalement.collier === 'Oui'
  let date = signalement.date
  date = date.split('/')
  signalement.date = date[2] + '-' + date[1] + '-' + date[0]
  signalementModifDisplay(signalement)
}

statutsProm.then(function(statuts){
  statuts.forEach(statut => {
    const option = "<option value=" + statut.id + ">" + statut.nom + "</option>\n"
    statutDOM.innerHTML += option
  });
})

brigadesProm.then(function(brigades){
  brigades.forEach(brigade => {
    const option = '<option value="' + brigade.nom + '">' + brigade.nom + '</option>\n'
    brigadeDOM.innerHTML += option
  });
})

brigadeDOM.addEventListener('change', function(e){
  if(brigadeDOM.value === 'tous') return listSignalementsDisplay(true)
  const signalements = pullSignalements().then( signalements => {
    return signalements.filter( function(signalement){ return signalement.brigade && signalement.brigade === brigadeDOM.value  })
  })
  listSignalementsDisplay(true, signalements)
})

statutDOM.addEventListener('change', function(e){
  listSignalementsDisplay(true)
})

listSignalementsDisplay()